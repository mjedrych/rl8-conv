# Rl8-Conv

Simple program that converts closed format Rl8 files (store information send from weather stations in National Museum in Krakow) into text files. The most difficulty was to reverse engineer Rl8 binary format.