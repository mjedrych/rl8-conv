#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iomanip>
#include <windows.h>
#include <time.h>

using namespace std;

int main(int argc, char* argv[]) {

	char* buffer = new char[4];
	char* bufferInt = new char[8];
	char* tmp = buffer;
	char* tmpInt = bufferInt;
	float Val;
	long long intVal;
	buffer = reinterpret_cast<char*>(&Val);
	bufferInt = reinterpret_cast<char*>(&intVal);

	string filename = "GORNA_GALERIA_SALA_1A11.rl8";
	//string filename = "PARTER_MAGAZYN_311.rl8";
	ifstream f(filename.c_str(), ios::binary);
	f.seekg(0, ios::end);
	const int fileLength = static_cast<int>(f.tellg());
	f.seekg(0, ios::beg);

	// allocate memory:
	char* bufferImageData;
	bufferImageData = new char[fileLength];

	// read data as a block:
	f.read((char*)bufferImageData, fileLength);
	f.close();
	f.clear();
	ofstream tempfile("temperature.txt");

	time_t     now;
	struct tm  ts;
	char       buf[80];

	for (int counter = 897; counter < fileLength; counter += 7*4) {
		for (int i = 0; i < 8; ++i)
			bufferInt[i] = 0;
		for(int i=0; i<4;++i)
			buffer[i] = bufferImageData[counter + i];
		float temp = Val;
		for (int i = 0; i<4; ++i)
			buffer[i] = bufferImageData[counter + 14 + i];
		float rh = Val;
		for (int i = 0; i<8; ++i)
			bufferInt[i] = bufferImageData[counter  + 4  + i];
		
		// 4675693452183207936 - 10 paz 2008
		
		//1337166000 = 4675874069874562389.0
		//Human time(GMT) : Wednesday, May 16, 2012 11 : 00 : 00 AM
		//Human time(your time zone) : Wednesday, May 16, 2012 1 : 00 : 00 PM GMT + 02 : 00
		//https://www.epochconverter.com/#tools

		double time = (intVal - 4675874069874562389.0)/ 5726623061.0*3600 + 1337166000.0 + 2*3600;
		//double time = (intVal / 5726623061) * 3600.0;
		now = (time_t)(ceil(time));
		gmtime_s(&ts, &now);
		// Format time, "ddd yyyy-mm-dd hh:mm:ss zzz"
		//strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", &ts);
		strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M", &ts);
		tempfile << std::fixed << buf << std::setprecision(1) << "\t" <<temp <<"\t"<< std::fixed << std::setprecision(1)<<rh << endl;
		
	}
	
	tempfile.close();
	tempfile.clear();
	delete[] bufferImageData;
	delete[] tmp;
	std::system("pause");


	return 0;
}


